# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://nj6338@bitbucket.org/nj6338/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/nj6338/stroboskop/commits/a23836e61ef2d884502f35a949724b5e6f6cd6e7

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/nj6338/stroboskop/commits/cb6a42920c0bc058a7439a2828a821c0e62dbef4

Naloga 6.3.2:
https://bitbucket.org/nj6338/stroboskop/commits/c39d2d39e562945c056aa47811e2b1c5dad8711a

Naloga 6.3.3:
https://bitbucket.org/nj6338/stroboskop/commits/85d802c31f4ea1da9a2b892e110355b1c3714b45

Naloga 6.3.4:
https://bitbucket.org/nj6338/stroboskop/commits/605a76ec12eefc36092e8051d2b707a1fd97d176

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/nj6338/stroboskop/commits/68162f445f5041dbb5a455020cb6794f3026878b

Naloga 6.4.2:
https://bitbucket.org/nj6338/stroboskop/commits/b2b5ac4ab838f513e21be80a8163c82ddceaf485

Naloga 6.4.3:
https://bitbucket.org/nj6338/stroboskop/commits/72dd73ac776a3e0566595ed0695b7bd3300e202b

Naloga 6.4.4:
https://bitbucket.org/nj6338/stroboskop/commits/279ef54702035c2ca6dd6b978caf1b0dec54370a